import axios from "axios";

export default axios.create({
  baseURL: "https://sandbox-api.layup.co.za" //uat
  //baseURL: "https://api.layup.co.za" //prod
});
