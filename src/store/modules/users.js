//import Vue from "native-base";
import axios from "axios";
import { AsyncStorage } from "react-native";
//import { Platform } from "react-native"
const baseURL = "https://sandbox-api.layup.co.za/"; //uat
//const baseURL = "https://api.layup.co.za/"; //prod
export default {
  namespaced: true,

  state: {
    user: [],
    validated:{}
  },
  actions: {
    verifyuser({ commit, state }, userData) {
      //alert ('from verifyuser userdata = ' + userData)
      //alert('from orders token is  ' + ApiKey);
      const url = baseURL + "v1/verify?" + userData.userdata //token=81257&msisdn=27760210593"
      return axios
 
        .get( url)

        .then(res => {
          const Validated = res.data;
          const ValidatedString = JSON.stringify(Validated);
         //alert(ValidatedString)
          AsyncStorage.setItem("validated", ValidatedString);
          
         // alert(Merchant.percFee)
          commit("setValidated",ValidatedString);
          return state.validated;
        })
        .catch(function(error) {
         // alert("from verifyuser :" + error.response.data);
          // handle error
          console.log(error);
          const Validated = "{}";
          AsyncStorage.setItem("validated", Validated);
          commit("setValidated", Validated);
          return state.validated;
        });
    },
  
    createuser({ commit, state }, userData) {
      
      //alert(userData.cellNumber) ;                  
      return axios

        .post(baseURL + "v1/auth/register", userData.cellNumber,{
          headers: {
            'dashpayN910': `${userData.orderref}`
         }
       })

        .then(res => {
          const User = res.data;
          const UserString = JSON.stringify(User);
          //alert(UserString);
          AsyncStorage.setItem("user", UserString);
          //AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setUser", UserString);
          return state.user;
        })
        .catch(function(error) {
          //alert("from createuser :" + error.response.status);
          // handle error
          console.log(error);
          const User = "[]";
          AsyncStorage.setItem("user", User);
          //AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setUser", User);
          return state.user;
        });
    },
    existinguser({ commit, state }, userData) {
      
      //alert('from orders token is  ' + ApiKey);
      AsyncStorage.setItem("userexists", "false");
      return axios

        .post(baseURL + "v1/tokens",userData.postdata,{
          headers: {
            'dashpayN910': `${userData.orderref}`
         }
       })

        .then(res => {
          const status = res.status;
          if (status == "204"){
            AsyncStorage.setItem("userexists", "true");
            const cellno = userData.cellNumber + ""
            
            const User = {
              cellNumber : cellno
            }
            const UserString = JSON.stringify(User);
            AsyncStorage.setItem("user", UserString);
          }
          
          return state.user;
        })
        .catch(function(error) {
          //alert("from existinguser :" + error.response.status);
          const status = error.response.status
          if (status == "400"){
            AsyncStorage.setItem("userexists", "false");
            const UserString = "{}";
            AsyncStorage.setItem("user", UserString);
          }
           return state.user;
        });
    },
    getmerchant({ commit, state }, userData) {
      const userdata = userData.merchantid
      const orderref = userData.orderref
      return axios

      .get(baseURL + "v1/merchants/" + userdata,{
        headers: {
          'dashpayN910': `${orderref}`
       }
     })

        .then(res => {
          const Merchant = res.data;
          const MerchantString = JSON.stringify(Merchant);
          AsyncStorage.setItem("merchant", MerchantString);
          if (Merchant.active == true) {
            AsyncStorage.setItem("layup-merchantactive", "true");
          } else {
            AsyncStorage.setItem("layup-merchantactive", "false");
          }
          AsyncStorage.setItem("layup-merchantpercfee", Merchant.percFee); 
         // alert(Merchant.percFee)
          commit("setMerchant", MerchantString);
          return state.merchant;
        })
        .catch(function(error) {
         // alert("from getmerchant :" + error.response.status);
          // handle error
          console.log(error);
          const Merchant = "[]";
          AsyncStorage.setItem("merchant", Merchant);
          AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setMerchant", Merchant);
          return state.merchant;
        });
    }
  },
  mutations: {
    setUser(state, user) {
      return (state.user = user);
    },
    setValidated(state, validated) {
      return (state.validated = validated);
    }
  }
};
