//import Vue from "native-base";
import axios from "axios";
import { AsyncStorage } from "react-native";
//import { Platform } from "react-native"
const baseURL = "https://sandbox-api.layup.co.za/"; //uat
//const baseURL = "https://api.layup.co.za/"; //prod
export default {
  namespaced: true,

  state: {
    plans: "{}",
    payplan: "{}"
  },
  actions: {
    getplan({ commit, state }, userData){
   //alert ('from verifyuser userdata = ' + userData)
      //alert('from orders token is  ' + ApiKey);
      const url = baseURL + "v1/payment-plan/" + userData.userdata //88152?msisdn=27760210593&populate=order,benefactor
      return axios
 
        .get( url,{
          headers: {
            'dashpayN910': `${userData.orderref}`
         }
       })

        .then(res => {
          const payplan = res.data;
          const PayPlanString = JSON.stringify(payplan);
         //alert(ValidatedString)
          AsyncStorage.setItem("payplan", PayPlanString);
          
         // alert(Merchant.percFee)
          commit("setPayPlan",PayPlanString);
          return state.payplan;
        })
        .catch(function(error) {
          //alert("from getplan :" + error.response.status);
          // handle error
          console.log(error);
          const payplan = "{}";
          AsyncStorage.setItem("payplan","{}");
          commit("setPayPlan", "{}");
          return state.payplan;
        });
    },
  
    createplan({ commit, state }, planData) {
      //alert('from orders token is  ' + ApiKey);
      return axios

        .post(baseURL + "v1/payment-plan", planData.paymentplan,{
          headers: {
            'dashpayN910': `${planData.orderref}`
         }
       })

        .then(res => {
          const Plan = res.data;
          const PlanString = JSON.stringify(Plan);
          AsyncStorage.setItem("plans", PlanString);
          //AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setPlans", PlanString);
          return state.plans;
        })
        .catch(function(error) {
          alert("from createplan :" + error.response.status);
          // handle error
          console.log(error);
          const Plan = "[]";
          AsyncStorage.setItem("plans", {Plan});
          //AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setPlans", Plan);
          return state.plans;
        });
    },
    
    plandetails({ commit, state },userData) {
      //alert('from orders token is  ' + ApiKey);
      return axios
        .post(baseURL + "v1/payment-plan/preview", userData.mypayload,{
          headers: {
            'dashpayN910': `${userData.orderref}`
         }
       })
        
        
        .then((res) => {
         
          const Plans = JSON.stringify(res.data);
          //const Orders = res.data.orders;
          
           if(Plans == undefined){
            //alert("undefined");
            Plans = "{}";
           }
           AsyncStorage.setItem("plans", Plans);
        
          commit("setPlans", Plans);
          
         // alert('getsummary orders follow:' + JSON.stringify(res.data.orders));
          return state.plans;
        })
        .catch(function(error) {
          //alert("from plandetails :" + error.response.status);
          // handle error
          console.log(error);
          const Plans = "[]";
          //alert(Orders);
          AsyncStorage.setItem("plans",Plans);
          commit("setPlans", Plans);
          
          return state.plans;
        });
    },
  },
  mutations: {
    setPlans(state, plans) {
      return (state.plans = plans);
    },
    setPayPlan(state, payplan) {
      return (state.payplan = payplan);
    },
  },
};
