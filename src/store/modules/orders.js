//import Vue from "native-base";
import axios from "axios";
import { AsyncStorage } from "react-native";
const baseURL = "https://sandbox-api.layup.co.za/"; //uat
//const baseURL = "https://api.layup.co.za/"; //prod
export default {
  namespaced: true,

  state: {
    orders: "[]",
    payments: "[}"
  },
  actions: {
    createorder({ commit, state }, orderData) {
      const orderdata = orderData.orderdata;
      const orderref = orderData.orderref;
      return axios

        .post(baseURL + "v1/orders", orderdata,{
          headers: {
            'dashpayN910': `${orderref}`
         }
       })

        .then(res => {
          const Order = res.data;
          const OrderString = JSON.stringify(Order);
          AsyncStorage.setItem("orders", OrderString);
          //AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setOrders", OrderString);
          return state.orders;
        })
        .catch(function(error) {
          alert("from createorder :" + error.response.status);
          // handle error
          console.log(error);
          const Order = "[]";
          AsyncStorage.setItem("orders", Orders);
          //AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setOrders", Order);
          return state.orders;
        });
    },
    attachorder({ commit, state }, selectData) {
     
      return axios

        .get(
          baseURL + "v1/orders/attach/" + selectData.orderid,{
            headers: {
              'apikey': `${selectData.apikey}`
           }
         }
          
        )
        .then(res => {
          //alert("from attachorder : Done");
          
        })
        .catch(function(error) {
         // alert("from attachorder :" + error.response.status);
          let orderid  = 557912324325435311;
          let merchantid = 234581296744467232;
          let invoiceno = orderid.toString(16);
          let referenceno = merchantid.toString(16);
          AsyncStorage.setItem("orderref", invoiceno.toUpperCase() + referenceno.toUpperCase());
          //alert(invoiceno.toUpperCase()+ referenceno.toUpperCase());
          
        });
    },

    getsummary({ commit, state }, selectData) {
      AsyncStorage.setItem("orders", "[]");
      const selectdata = selectData.selectdetails;
      const orderref = selectData.orderref;
      
      return axios

        .get(
          baseURL + "v1/orders?" + selectdata,{
          headers: {
            'dashpayN910': `${orderref}`
         }
       })
          
        .then(res => {
          const Orders = JSON.stringify(res.data.orders);
          //const Orders = res.data.orders;

          if (Orders == undefined) {
            //alert("undefined");
            Orders = "[]";
          }
          AsyncStorage.setItem("orders", Orders);

          commit("setOrders", Orders);

          // alert('getsummary orders follow:' + JSON.stringify(res.data.orders));
          return state.orders;
        })
        .catch(function(error) {
          //alert("from getsummary :" + error.response.status);
          // handle error
          console.log(error);
          const Orders = "[]";
          //alert(Orders);
          AsyncStorage.setItem("orders", Orders);
          commit("setOrders", Orders);

          return state.orders;
        });
    }
  },
  mutations: {
    setOrders(state, orders) {
      return (state.orders = orders);
    },
    setPayments(state, payments) { 
      return (state.payments = payments);
    } 
  }
};
