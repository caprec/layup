import axios from "axios";
import { AsyncStorage } from "react-native";
const baseURL = "https://sandbox-api.layup.co.za/"; //uat
//const baseURL = "https://api.layup.co.za/"; //prod
export default {
  namespaced: true,

  state: {
    merchant: []
  },
  actions: {
    createmerchant({ commit, state }, userData) {
      const userdata = {
        name: userData.name,
        domain: userData.domain
      };
      const orderref = userData.orderref;
      return axios

        .post(baseURL + "v1/merchants", userdata,{
          headers: {
            'dashpayN910': `${orderref}`
         }
       })

        .then(res => {
          const Merchant = res.data;
          const MerchantString = JSON.stringify(Merchant);
          AsyncStorage.setItem("merchant", MerchantString);
          AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setMerchant", MerchantString);
          return state.merchant;
        })
        .catch(function(error) {
          //alert("from createmerchant :" + error.response.status);
          // handle error
          console.log(error);
          const Merchant = "[]";
          AsyncStorage.setItem("merchant", Merchant);
          AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setMerchant", Merchant);
          return state.merchant;
        });
    },
    getmerchant({ commit, state }, userData) {
      const userdata = userData.merchantid
      const orderref = userData.orderref
           
      return axios

        .get(baseURL + "v1/merchants/" + userdata,{
          headers: {
            'dashpayN910': `${orderref}`
         }
       })

        .then(res => {
          const Merchant = res.data;
          const MerchantString = JSON.stringify(Merchant);
          AsyncStorage.setItem("merchant", MerchantString);
          if (Merchant.active == true) {
            AsyncStorage.setItem("layup-merchantactive", "true");
          } else {
            AsyncStorage.setItem("layup-merchantactive", "false");
          }
          AsyncStorage.setItem("layup-merchantpercfee", Merchant.percFee.toString()); 
         // alert(Merchant.percFee.toString())
          commit("setMerchant", MerchantString);
          return state.merchant;
        })
        .catch(function(error) {
         // alert("from getmerchant :" + error.response.status);
          // handle error
          console.log(error);
          const Merchant = "[]";
          AsyncStorage.setItem("merchant", Merchant);
          AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setMerchant", Merchant);
          return state.merchant;
        });
    }
  },
  mutations: {
    setMerchant(state, merchant) {
      return (state.merchant = merchant);
    }
  }
};
