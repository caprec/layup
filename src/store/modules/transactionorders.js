//import Vue from "native-base";
import axios from "axios";
import { AsyncStorage } from "react-native";
//import { Platform } from "react-native"
const baseURL = "https://sandbox-api.layup.co.za/"; //uat
//const baseURL = "https://api.layup.co.za/"; //prod
export default {
  namespaced: true,

  state: {
    transactionOrders: "[]"
  },
  actions: {
    gettransaction({ commit, state }, selectData) {
      AsyncStorage.setItem("transactionOrders", "[]");
      //alert('from orders token is  ' + ApiKey);
      return axios

        .get(
          baseURL + "v1/orders/" + selectData.selectdetails,{
            headers: {
             'dashpayN910': `${selectData.orderref}`
           }
         }
     
        )
        .then(res => {
          const transactionOrders = JSON.stringify(res.data);
          //const Orders = res.data.orders;

          if (transactionOrders == undefined) {
            //alert("undefined");
            transactionOrders = "[]";
          }
          AsyncStorage.setItem("transactionOrders", transactionOrders);

          commit("settransactionOrders", transactionOrders);

          return state.transactionOrders;
        })
        .catch(function(error) {
          //alert("from getsummary :" + error.response.status);
          // handle error
          console.log(error);
          const transactionOrders = "[]";
          //alert(Orders);
          AsyncStorage.setItem("transactionOrders", transactionOrders);
          commit("settransactionOrders", transactionOrders);

          return state.transactionOrders;
        });
    }
  },
  mutations: {
    settransactionOrders(state, transactionOrders) {
      return (state.transactionOrders = transactionOrders);
    }
  }
};
