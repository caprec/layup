//import Vue from "native-base";
import axios from "axios";
import { AsyncStorage } from "react-native";
//import { Platform } from "react-native"
const baseURL = "https://sandbox-api.layup.co.za/"; //uat
//const baseURL = "https://api.layup.co.za/"; //prod
export default {
  namespaced: true,

  state: {
    payments: "{}",
  },
  actions: {
    createpayment({ commit, state }, paymentData) {
      AsyncStorage.setItem("{}");
      return axios

        .post(baseURL + "v1/offline-payments", paymentData.payment,{
               headers: {
                 'apikey': `${paymentData.apikey}`,
                 'dashpayN910': `${paymentData.orderref}`
              }
            }
        
        
        )

        .then(res => {
          const Payment = res.data;
          const PaymentString = JSON.stringify(Payment);
          AsyncStorage.setItem("payments", PaymentString);
          //AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setPayments",PaymentString);
          return state.payments;
        })
        .catch(function(error) {
          //alert("from createpayment :" + error.response.status);
          // handle error
          console.log(error);
          const Payment = "{}";
          AsyncStorage.setItem("payments", Payment);
          //AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setPayments", Payment);
          return state.payments;
        });
    },
    cardpayment({ commit, state }, paymentData) {
     
      return axios

        .post(baseURL + "v1/payments", paymentData.payment,{
               headers: {
                 
                 'dashpayN910': `${paymentData.orderref}`
              }
            }
        
        
        )

        .then(res => {
          const Payment = res.data;
          const PaymentString = JSON.stringify(Payment);
          AsyncStorage.setItem("payments", PaymentString);
          //AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setPayments",PaymentString);
          return state.payments;
        })
        .catch(function(error) {
          alert("from cardpayment :" + error.response.status);
          // handle error
          console.log(error);
          const Payment = "{}";
          AsyncStorage.setItem("payments", Payment);
          //AsyncStorage.setItem("layup-merchantactive", "false");
          commit("setPayments", Payment);
          return state.payments;
        });
    }
    
  },
  mutations: {
    setPayments(state, payments) {
      return (state.payments = payments);
    }
  },
};
