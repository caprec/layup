import axios from "axios";
import { Alert, AsyncStorage } from "react-native";
const baseURL = "https://sandbox-api.layup.co.za/"; //uat
//const baseURL = "https://api.layup.co.za/"; //prod
export default {
  namespaced: true,

  state: {
    user: {},
    merchant: {},
    isAuth: "false",
    isMerchant: "false",
    userVerified: "false",
    merchantVerified: "false",
    ApiKey: null,
  },
  actions: {
    login({ commit, state }, userData) {
      //alert(baseURL);
      const orderref = userData.orderref;
      const userdata =  {username: userData.username,
                         password: userData.password }
    
      AsyncStorage.setItem("layup-email", userdata.username); 
      return axios
        .post(baseURL + "v1/auth/login", userdata,{
          headers: {
            'dashpayN910': `${orderref}`
         }
       })
        .then((res) => {
          const user = res.data;
          //alert(JSON.stringify(res.data));
          AsyncStorage.setItem("layup-login-error", "");
          AsyncStorage.setItem("layup-id", user._id);
          AsyncStorage.setItem("layup-name", user.name);
          AsyncStorage.setItem("layup-isAuth", "true");
          //alert(AsyncStorage.getItem("layup-name"));
          if (user.verified == true) {
            AsyncStorage.setItem("layup-userverified", "true");
          } else {
            AsyncStorage.setItem("layup-userverified", "false");
          }
          AsyncStorage.setItem("layup-email", user.email); 
          if (user.merchantId != undefined){
            AsyncStorage.setItem("layup-ismerchant", "true");
            AsyncStorage.setItem("layup-merchantid", user.merchantId);
         // alert(user.merchantId);
          }else{
            AsyncStorage.setItem("layup-ismerchant", "false");
            AsyncStorage.setItem("layup-merchantid", "");
          //  alert('Not registered as a Merchant yet !');
          }
          commit("setAuthUser", user);
          commit("setAuth", "true");
          //commit("setMerchant", "false")
          return state.user;
        })
        .catch(function(error) {
          // handle error
          console.log(error);
          
          AsyncStorage.setItem("layup-login-error", error.response.data)
          //alert(error.response.data)
          const user = {};
          AsyncStorage.setItem("layup-id", "");
          AsyncStorage.setItem("layup-name", "");
          AsyncStorage.setItem("layup-isAuth", "false");
          AsyncStorage.setItem("layup-userverified", "false");
          AsyncStorage.setItem("layup-merchantid", "");
          commit("setAuthUser", user);
          commit("setAuth", "false");
          
          return state.user;
        });
    },
    gettoken({ commit, state },orderref) {
      
      return axios
        .get(baseURL + "v1/auth/apikey",{
          headers: {
            'dashpayN910': `${orderref}`
         }
       })
        .then((res) => {
          const apikey = res.data.apikey;
          AsyncStorage.setItem("layup-token", apikey);
          commit("setApiKey", apikey);
          //alert('from store:' + apikey)
          return state.ApiKey;
        })
        .catch(function(error) {
         // alert(error.response.status);
          // handle error
          console.log(error);
          const apikey = null;
          AsyncStorage.setItem("layup-token", apikey);
          commit("setApiKey", apikey);
          return state.ApiKey;
        });
    },
  },
  mutations: {
    setAuthUser(state, user) {
      return (state.user = user);
    },
    setuserVerified(state, userVerified) {
      return (state.userVerified = userVerified);
    },
    setAuthMerchant(state, merchant) {
      return (state.merchant = merchant);
    },
    setmerchantVerified(state, merchantVerified) {
      return (state.merchantVerified = merchantVerified);
    },
    setAuth(state, isAuth) {
      return (state.isAuth = isAuth);
    },
    setisMerchant(state, isMerchant) {
      return (state.isMerchant = isMerchant);
    },
    setApiKey(state, ApiKey) {
      return (state.ApiKey = ApiKey);
    },
  },
};
