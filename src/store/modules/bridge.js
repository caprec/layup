//import Vue from "native-base";
import axios from "axios";
import { AsyncStorage } from "react-native";
//import { Platform } from "react-native"
const merchantnum = "455077300001435"; //uat
//const merchantnum = "???"; //prod
export default {
  namespaced: true,

  state: {
    transactionOrders: "[]"
  },
  actions: {
    swipecard({ commit, state }, selectData) {
      //AsyncStorage.setItem("transactionOrders", "[]");
      //alert('from orders token is  ' + ApiKey);
      return axios

        .get( 
         //"http://192.168.0.150:9000/runOtherApp?key=ReadCard" + selectData
        "http://127.0.0.1:9000/runOtherApp?key=ReadCard" + selectData
        
        )
        .then(res => {
          const cardDetails = JSON.stringify(res.data);
          alert(cardDetails);
         
        })
        .catch(function(error) {
          alert("from ReadCard : error");
          // handle error
          console.log(error);
        
        });
    },
    getpayment({ commit, state }, selectData) {
      //AsyncStorage.setItem("transactionOrders", "[]");
      //alert('from orders token is  ' + ApiKey);
      AsyncStorage.setItem("cardDetails", ' {"RESULT":"ERROR"}  ');
      return axios

        .get( 
        //"http://192.168.0.150:9000/runOtherApp?appID=com.ar.dashpaypos&amount=" + selectData + "&transtype=PURCHASE&merchantnum=435011001786821"
        //"http://127.0.0.1:9000/runOtherApp?appID=com.ar.dashpaypos&amount=" + selectData + "&transtype=PURCHASE&merchantnum=435011001786821" 
        
        
        "http://127.0.0.1:9000/runOtherApp?appID=com.ar.dashpaypos&amount=" + selectData + "&transtype=PURCHASE&merchantnum=" + merchantnum 
         
        
        )
        .then(res => {
          const cardDetails = JSON.stringify(res.data);
          AsyncStorage.setItem("cardDetails", cardDetails);
          //alert(cardDetails);
         
        })
        .catch(function(error) {
          AsyncStorage.setItem("cardDetails", ' {"RESULT":"ERROR"}  ');
         // alert("from getpayment : error");
          // handle error
          console.log(error);
        
        });
    },
    scancode({ commit, state }, selectData) {
      //AsyncStorage.setItem("transactionOrders", "[]");
      //alert('from orders token is  ' + ApiKey);
      return axios

        .get( 
         //"http://192.168.0.150:9000/runOtherApp?key=ScanCode" + selectData
         "http://127.0.0.1:9000/runOtherApp?key=ScanCode" + selectData
        
        )
        .then(res => {
          const cardDetails = JSON.stringify(res.data);
          alert(cardDetails);
         
        })
        .catch(function(error) {
          alert("from ScanCode : error");
          // handle error
          console.log(error);
        
        });
    },
    printslip({ commit, state }, selectData) {
      //AsyncStorage.setItem("transactionOrders", "[]");
      //alert('from orders token is  ' + ApiKey);
      return axios

        .get( 
         //"http://192.168.0.150:9000/runOtherApp?key=Print&printString=" + encodeURI(selectData)
         "http://127.0.0.1:9000/runOtherApp?key=Print&printString=" + encodeURI(selectData)
          
        //  
        )
        .then(res => {
          //const cardDetails = JSON.stringify(res.data);
          //alert(cardDetails);
         
        })
        .catch(function(error) {
          alert("from printSlip : error");
          // handle error
          console.log(error);
        
        });
    },
  },
  mutations: {
    settransactionOrders(state, transactionOrders) {
      return (state.transactionOrders = transactionOrders);
    }
  }
};
