import Vuex from 'vuex'
import Vue from 'vue-native-core'
import auth from './modules/auth'
import orders from './modules/orders'
import transactionorders from './modules/transactionorders'
import merchants from './modules/merchants'
import users from './modules/users'
import plans from  './modules/plans'
import payments from  './modules/payments'
import bridge from  './modules/bridge'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
    auth,
    orders,
    transactionorders,
    merchants,
    users,
    plans,
    payments,
    bridge
    },
    state: {

    },
    getters: {

    },
    actions: {

    },
    mutations: {
        
    }
})

